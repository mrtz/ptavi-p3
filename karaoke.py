#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import sys
import json
import urllib.request


class Karaokelocal:

    def __init__(self, fichero):

        parser = make_parser()
        # es el elemento tonto que va leyendo linea a linea
        # y cuando salta un evento llama a nuestro manejador
        cHandler = SmallSMILHandler()
        # creo de la clase un objeto cHandler, que será el manejador
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        # me va a parsear el fichero(smil)
        self.list = cHandler.get_tags()

    def __str__(self):

        salida = ""
        for elemento in self.list:
            nombreeti = elemento[0]
            etiqueta = elemento[1]
            salida += '\n' + nombreeti + '\t'
            for atributo in etiqueta:
                self.valor = etiqueta[atributo]
                if self.valor != '':
                    salida += atributo + '=' + self.valor + '' + '\t'

        return (salida)

    def to_json(self, fichero, ficherojson=''):
        if ficherojson == '':
            ficherojson = fichero.replace('.smil', '.json')
            # cambio de extension
            with open(fichero, 'w') as file:
                json.dump(self.list, file)

    def do_local(self):

        for elemento in self.list:
            etiqueta = elemento[1]

            for atributo in etiqueta:
                if atributo == 'src':
                    if self.valor.startswith('http://'):
                        url = self.valor
                        file = url.split('/')[-1]
                        urllib.request.urlretrieve(url, file)


if __name__ == "__main__":

    try:
        fichero = sys.argv[1]  # si no se especifica fichero

    except IndexError:
        sys.exit('usage: python3 karaoke.py file.smil')

    karaoke = Karaokelocal(fichero)
    # instancia un objeto de la clase Karaokelocal
    print(karaoke)
    karaoke.to_json(fichero)
    karaoke.do_local()
    karaoke.to_json(fichero, 'local.json')
    print(karaoke)
