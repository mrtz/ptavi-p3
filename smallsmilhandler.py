# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        self.root = {}  # error si pongo root-layout
        self.region = {}
        self.img = {}
        self.audio = {}
        self.textstream = {}
        # 5 diccionarios, 1x cada etiqueta, dentro los atr con sus valores
        self.content = []
        # lista vacía donde voy a almacenar las etiquetas
        # con sus atributos y el contenido de los atributos

    def startElement(self, name, attrs):
        if name == 'root-layout':
            # De esta manera tomamos los valores de los atributos
            self.root['width'] = attrs.get('width', "")
            self.root['height'] = attrs.get('height', "")
            self.root['background-color'] = attrs.get('background-color', "")
            # width, height y back son los atributos
            # "" en caso de que el atributo no tenga contenido, queda vacío
            self.content.append([name, self.root])
            # la lista guarda el name + los 'atributos':'valores'
            self.root = {}

        elif name == 'region':
            self.region['id'] = attrs.get('id', "")
            self.region['top'] = attrs.get('top', "")
            self.region['bottom'] = attrs.get('bottom', "")
            self.region['left'] = attrs.get('left', "")
            self.region['right'] = attrs.get('right', "")
            self.content.append([name, self.region])
            self.region = {}

        elif name == 'img':
            self.img['src'] = attrs.get('src', "")
            self.img['region'] = attrs.get('region', "")
            self.img['begin'] = attrs.get('begin', "")
            self.img['dur'] = attrs.get('dur', "")
            self.content.append([name, self.img])
            self.img = {}

        elif name == 'audio':
            self.audio['src'] = attrs.get('src', "")
            self.audio['begin'] = attrs.get('begin', "")
            self.audio['dur'] = attrs.get('dur', "")
            self.content.append([name, self.audio])
            self.audio = {}

        elif name == 'textstream':
            self.textstream['src'] = attrs.get('src', "")
            self.textstream['region'] = attrs.get('region', "")
            self.content.append([name, self.textstream])
            self.textstream = {}

    def get_tags(self):
        return self.content


# podría haber creado en el init 1 solo diccionario vacío
# self.dicc={}
# y meter cada atributo en ese diccionario
# self.dicc['dur']=attrs.get('dur', "")

if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
